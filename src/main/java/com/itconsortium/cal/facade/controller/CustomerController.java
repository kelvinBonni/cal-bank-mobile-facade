package com.itconsortium.cal.facade.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.itconsortium.cal.facade.model.Customer;
import com.itconsortium.cal.facade.repository.CustomerRepository;
import com.itconsortium.cal.facade.util.properties.PlatformEndpoints;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/customer")
@Slf4j
public class CustomerController {
	@Autowired
	CustomerRepository customerRepository;
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	PlatformEndpoints platformEndpoints;

	Map<String, String> params = new HashMap<String, String>();
	@PostMapping("/create")
	public Customer create(@RequestBody Customer customer, HttpServletResponse response) {
		Customer createdCustomer = customerRepository.save(customer);
		if (createdCustomer != null) {
			String url = platformEndpoints.getCreateCustomerEndpoint();
			restTemplate.postForObject(url, customer, Customer.class);
		}
		return createdCustomer;
	}
	
	@GetMapping("/find/{msisdn}")
	public Customer findByMsisdn(@PathVariable String msisdn, HttpServletResponse response){
		String url = platformEndpoints.getFindCustomerEndpoint();			
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
				.queryParam("msisdn", msisdn);
		HttpEntity<?> entity = new HttpEntity<>(headers);
		HttpEntity<String> response1 = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, String.class);		
		return customerRepository.findByMsisdn(msisdn);
	}
	
	@DeleteMapping("/delete/{msisdn}")
	public String delete(@PathVariable String msisdn, HttpServletResponse response){
		Customer customer = customerRepository.findByMsisdn(msisdn);
		String url = platformEndpoints.getDeleteCustomerEndpoint();
		customerRepository.delete(customer);
		Map<String, String> params = new HashMap<String, String>();
		params.put("msisdn",msisdn);
		restTemplate.delete(url, params);		
		String deleteMessage = "Customer with id:"+customer.getId()+"has been deleted";
		return deleteMessage; 
	}
	
	
}