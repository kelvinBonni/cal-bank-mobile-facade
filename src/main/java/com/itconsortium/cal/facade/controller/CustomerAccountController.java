package com.itconsortium.cal.facade.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.itconsortium.cal.facade.model.Customer;
import com.itconsortium.cal.facade.model.CustomerAccount;
import com.itconsortium.cal.facade.repository.CustomerAccountRepository;
import com.itconsortium.cal.facade.repository.CustomerRepository;
import com.itconsortium.cal.facade.util.properties.PlatformEndpoints;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/account")
@Slf4j
public class CustomerAccountController {
	@Autowired
	CustomerRepository customerRepository;
	@Autowired
	CustomerAccountRepository customerAccountRepository;
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	PlatformEndpoints platformEndpoints;

	@PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
	public Customer create(@RequestBody CustomerAccount customerAccount) {
		Customer customer = customerRepository.findByMsisdn(customerAccount.getMsisdn());
		customer.addCustomerAccount(customerAccount);
		Customer result = customerRepository.save(customer);
		if (result  != null) {
			String url = platformEndpoints.getAddCustomerAccountEndpoint();
			restTemplate.postForObject(url, customerAccount, CustomerAccount.class);
		}
		return result;
	}
	
	@GetMapping("/find/{msisdn}")
	public CustomerAccount findByMsisdn(@PathVariable String msisdn, HttpServletResponse response){
		String url = platformEndpoints.getFindCustomerAccountEndpoint();	
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
				.queryParam("msisdn", msisdn);
		HttpEntity<?> entity = new HttpEntity<>(headers);
		HttpEntity<String> response1 = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, String.class);		
		return customerAccountRepository.findByMsisdn(msisdn);
	}
	
	@DeleteMapping("/delete/{msisdn}")
	public String delete(@PathVariable String msisdn, HttpServletResponse response){
		CustomerAccount customerAccount = customerAccountRepository.findByMsisdn(msisdn);
		customerAccountRepository.delete(customerAccount);
		String url = platformEndpoints.getDeleteCustomerAccountEndpoint();
		Map<String, String> params = new HashMap<String, String>();
		params.put("msisdn",msisdn);
		restTemplate.delete(url, params);	
		String deleteMessage = "Customer with id:"+customerAccount.getId()+"has been deleted";
		return deleteMessage; 
	}
}
