package com.itconsortium.cal.facade.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Entity
@Table
@Slf4j
@Data
public class Customer {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<CustomerAccount> customerAccounts;

	@Column(length = 30)
	private String msisdn;
	@Enumerated(EnumType.STRING)
	private CustomerStatus customerStatus;

	@Column(length = 100)
	@NotNull
	private String firstName;
	@Column(length = 100)
	@NotNull
	private String lastName;
	@Column(length = 100)
	private String otherNames;

	@Column(length = 100)
	private String createdBy;

	@Column(length = 100)
	private String authorizedBy;

	@Column(length = 100)
	private String deauthorizedBy;

	public void addCustomerAccount(CustomerAccount customerAccount) {
		if (customerAccount != null) {
			log.info("customer account is not null");
			if (customerAccounts == null) {
				customerAccounts = new ArrayList<CustomerAccount>();
			}
			customerAccounts.add(customerAccount);
			customerAccount.setCustomer(this);
		} else {
			return;
		}
	}

}
