package com.itconsortium.cal.facade.model;

public enum SrvAction {
	ACTIVATED, DEACTIVATED
}
