package com.itconsortium.cal.facade.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table
@Data
public class CustomerAccount {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "customerId", referencedColumnName = "id")
	@JsonIgnore
	private Customer customer;
	@Column(length = 30)
	@NotNull
	private String accountLabel;
	@Column
	private BigDecimal minBal;
	@Column
	private Integer maxDailyFrequency;
	@Column
	private BigDecimal maxDailyAmount;
	@Column(length = 30)
	private String msisdn;
	@Enumerated(EnumType.STRING)
	private SrvAction srvAction;
	@Column(length = 30)
	private String bankAccountNumber;
	@Column(length = 30)
	private String bankAccountBranch;
}
