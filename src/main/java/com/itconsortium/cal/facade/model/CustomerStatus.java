package com.itconsortium.cal.facade.model;
public enum CustomerStatus {
	ACTIVE,
	INACTIVE
}