package com.itconsortium.cal.facade;

import javax.persistence.Basic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class CalBankFacadeApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalBankFacadeApplication.class, args);
	}
	
	@Bean
	RestTemplate restTemplate(){
		return new RestTemplate();
	}
}
