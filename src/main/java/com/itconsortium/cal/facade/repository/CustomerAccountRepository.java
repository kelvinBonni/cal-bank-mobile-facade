package com.itconsortium.cal.facade.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itconsortium.cal.facade.model.CustomerAccount;

@Repository
public interface CustomerAccountRepository extends CrudRepository<CustomerAccount, Long>{
	CustomerAccount findByMsisdn(String msisdn);
}
