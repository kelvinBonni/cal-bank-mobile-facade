package com.itconsortium.cal.facade.util.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


import lombok.Data;

@Component
@Data
@ConfigurationProperties(prefix="platform")
public class PlatformEndpoints {
	private String createCustomerEndpoint;
	private String findCustomerEndpoint;
	private String deleteCustomerEndpoint;
	private String addCustomerAccountEndpoint;
	private String findCustomerAccountEndpoint;
	private String deleteCustomerAccountEndpoint;

}
